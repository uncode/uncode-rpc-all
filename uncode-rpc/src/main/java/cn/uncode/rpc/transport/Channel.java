package cn.uncode.rpc.transport;

import java.net.InetSocketAddress;

import cn.uncode.rpc.core.URL;
import cn.uncode.rpc.exception.TransportException;




/**
 * 
 * 类说明
 * 
 */

public interface Channel {
	
    URL getUrl();

    /**
     * get local socket address.
     * 
     * @return local address.
     */
    InetSocketAddress getLocalAddress();

    /**
     * get remote socket address
     * 
     * @return
     */
    InetSocketAddress getRemoteAddress();
    
    
    /**
     * open the channel
     * 
     * @return
     */
    boolean open();

    
    /**
     * send message.
     * 
     * @param message
     * @throws RemotingException
     */
    void write(Object message) throws TransportException;


    /**
     * close the channel.
     */
    void close();

    /**
     * close the channel gracefully.
     */
    void close(int timeout);

    /**
     * is closed.
     * 
     * @return closed
     */
    boolean isClosed();

    /**
     * the node available status
     * 
     * @return
     */
    boolean isAvailable();
    
    





}
