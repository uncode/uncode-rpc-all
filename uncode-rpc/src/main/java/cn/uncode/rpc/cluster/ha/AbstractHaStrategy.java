package cn.uncode.rpc.cluster.ha;

import cn.uncode.rpc.cluster.HaStrategy;
import cn.uncode.rpc.core.URL;

/**
 * 
 * Abstract ha strategy.
 *
 */

public abstract class AbstractHaStrategy<T> implements HaStrategy<T> {

    protected URL url;

    @Override
    public void setUrl(URL url) {
        this.url = url;
    }

}
