package cn.uncode.rpc.core;

/**
 * 服务提供，服务暴露
 * Export service providers.
 */

public interface Exporter<T> extends Node{

	Invoker<T> getInvoker();

    void unexport();
}
