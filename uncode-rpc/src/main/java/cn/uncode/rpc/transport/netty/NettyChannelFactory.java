/*
 *  Copyright 2009-2016 Weibo, Inc.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package cn.uncode.rpc.transport.netty;

import cn.uncode.rpc.core.URL;
import cn.uncode.rpc.spi.SpiMeta;
import cn.uncode.rpc.transport.Client;
import cn.uncode.rpc.transport.RequestHandler;
import cn.uncode.rpc.transport.Server;
import cn.uncode.rpc.transport.support.AbstractChannelFactory;

@SpiMeta(name = "uncode")
public class NettyChannelFactory extends AbstractChannelFactory {

	@Override
	protected Server innerCreateServer(URL url, RequestHandler requestHandler) {
		return new NettyServer(url, requestHandler);
	}

	@Override
	protected Client innerCreateClient(URL url) {
		return new NettyClient(url);
	}

}
