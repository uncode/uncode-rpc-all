
package cn.uncode.rpc.core;

/**
 * 
 * 服务调用方接口
 */
public interface Invoker<T> extends Node {

    Class<T> getInterface();
    
    /**
     * 当前使用该referer的调用数
     */
    int activeInvokeCount();
    
    Response invoke(Request request);

}
