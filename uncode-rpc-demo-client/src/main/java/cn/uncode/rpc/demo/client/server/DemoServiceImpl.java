package cn.uncode.rpc.demo.client.server;

import cn.uncode.rpc.demo.api.DemoService;
import cn.uncode.rpc.spring.annotation.Provider;

@Provider(interfaceClass=DemoService.class, export="8001")
public class DemoServiceImpl implements DemoService {

    public String hello(String name) {
        System.out.println(name);
        return "Server Hello " + name + "!";
    }

}
