/*
 *  Copyright 2009-2016 Weibo, Inc.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package cn.uncode.rpc.transport.netty;


import cn.uncode.rpc.common.CommonConstant;
import cn.uncode.rpc.common.log.Logger;
import cn.uncode.rpc.common.log.LoggerFactory;
import cn.uncode.rpc.core.DefaultResponse;
import cn.uncode.rpc.core.Request;
import cn.uncode.rpc.core.Response;
import cn.uncode.rpc.transport.Codec;
import cn.uncode.rpc.util.ByteUtil;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;



/**
 * @author maijunsheng
 * @version 创建时间：2013-5-31
 * 
 */
public class NettyEncoder extends MessageToByteEncoder<Object> {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(NettyEncoder.class);
	
	private Codec codec;
	private cn.uncode.rpc.transport.Channel client;

	public NettyEncoder(Codec codec) {
		this.codec = codec;
	}

	@Override
	protected void encode(ChannelHandlerContext ctx, Object message, ByteBuf out) throws Exception {
		
		long requestId = getRequestId(message);
		byte[] data = null;
		
		if (message instanceof Response) {
			try {
				data = codec.encode(client, message);
			} catch (Exception e) {
				LOGGER.error("NettyEncoder encode error, identity=" + client.getUrl().getIdentity(), e);
				Response response = buildExceptionResponse(requestId, e);
				data = codec.encode(client, response);
			}
		} else {
			data = codec.encode(client, message);
		}

		byte[] transportHeader = new byte[CommonConstant.NETTY_HEADER];
		ByteUtil.short2bytes(CommonConstant.NETTY_MAGIC_TYPE, transportHeader, 0);
		transportHeader[3] = getType(message);
		ByteUtil.long2bytes(getRequestId(message), transportHeader, 4);
		ByteUtil.int2bytes(data.length, transportHeader, 12);
		
		out.writeBytes(transportHeader);
		out.writeBytes(data);
//		return ChannelBuffers.wrappedBuffer(transportHeader, data);
	}

	private long getRequestId(Object message) {
		if (message instanceof Request) {
			return ((Request) message).getRequestId();
		} else if (message instanceof Response) {
			return ((Response) message).getRequestId();
		} else {
			return 0;
		}
	}

	private byte getType(Object message) {
		if (message instanceof Request) {
			return CommonConstant.FLAG_REQUEST;
		} else if (message instanceof Response) {
			return CommonConstant.FLAG_RESPONSE;
		} else {
			return CommonConstant.FLAG_OTHER;
		}
	}
	
	private Response buildExceptionResponse(long requestId, Exception e) {
		DefaultResponse response = new DefaultResponse();
		response.setRequestId(requestId);
		response.setException(e);
		return response;
	}
}
