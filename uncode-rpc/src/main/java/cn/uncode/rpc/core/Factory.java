package cn.uncode.rpc.core;

import java.lang.reflect.InvocationHandler;
import java.util.List;

import cn.uncode.rpc.cluster.ClusterSupport;
import cn.uncode.rpc.spi.Scope;
import cn.uncode.rpc.spi.Spi;


@Spi(scope = Scope.SINGLETON)
public interface Factory {
	
	<T> Exporter<T> getExport(Class<T> interfaceClass, T ref, List<URL> registryUrls);
	
	<T> T getProxy(Class<T> clz, InvocationHandler invocationHandler);
	
	<T> ClusterSupport<T> getClusterSupport(Class<T> interfaceClass, List<URL> registryUrls);
	

}
