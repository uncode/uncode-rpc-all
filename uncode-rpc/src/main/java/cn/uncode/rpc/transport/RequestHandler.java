package cn.uncode.rpc.transport;

import cn.uncode.rpc.core.Request;
import cn.uncode.rpc.exception.TransportException;
import cn.uncode.rpc.spi.Spi;

/**
 * ChannelHandler. (API, Prototype, ThreadSafe)
 */
@Spi
public interface RequestHandler {



    /**
     * on message sent.
     * 
     */
	Object handle(Request request) throws TransportException;

}