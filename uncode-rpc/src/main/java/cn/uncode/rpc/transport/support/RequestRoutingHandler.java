/*
 *  Copyright 2009-2016 Weibo, Inc.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package cn.uncode.rpc.transport.support;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import cn.uncode.rpc.common.log.Logger;
import cn.uncode.rpc.common.log.LoggerFactory;
import cn.uncode.rpc.core.DefaultResponse;
import cn.uncode.rpc.core.Invoker;
import cn.uncode.rpc.core.Request;
import cn.uncode.rpc.core.Response;
import cn.uncode.rpc.core.protocol.rpc.CompressRpcCodec;
import cn.uncode.rpc.exception.FrameworkException;
import cn.uncode.rpc.exception.TransportException;
import cn.uncode.rpc.transport.RequestHandler;
import cn.uncode.rpc.util.FrameworkUtil;
import cn.uncode.rpc.util.ReflectUtil;



/**
 * service 消息处理
 * 
 * <pre>
 * 		1） 多个service的支持
 * 		2） 区分service的方式： group/interface/version
 * </pre>
 * 
 * @author maijunsheng
 * @version 创建时间：2013-6-4
 * 
 */
public class RequestRoutingHandler implements RequestHandler {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(RequestRoutingHandler.class);
	
    private Map<String, Invoker<?>> providers = new HashMap<String, Invoker<?>>();

    // 所有暴露出去的方法计数
    // 比如：messageRouter 里面涉及2个Service: ServiceA 有5个public method，ServiceB
    // 有10个public method，那么就是15
    protected AtomicInteger methodCounter = new AtomicInteger(0);

    public RequestRoutingHandler() {}

    public RequestRoutingHandler(Invoker<?> provider) {
        addProvider(provider);
    }

    @Override
    public Object handle(Request request) {
        String serviceKey = FrameworkUtil.getServiceKey(request);
        Invoker<?> provider = providers.get(serviceKey);

        if (provider == null) {
        	LOGGER.error(this.getClass().getSimpleName() + " handler Error: provider not exist serviceKey=" + serviceKey + " "
                    + FrameworkUtil.toString(request));
        	TransportException exception = new TransportException(this.getClass().getSimpleName() + " handler Error: provider not exist serviceKey="
                            + serviceKey + " " + FrameworkUtil.toString(request));

            DefaultResponse response = new DefaultResponse();
            response.setException(exception);
            return response;
        }

        return call(request, provider);
    }

    protected Response call(Request request, Invoker<?> provider) {
        try {
            return provider.invoke(request);
        } catch (Exception e) {
            DefaultResponse response = new DefaultResponse();
            response.setException(new TransportException("provider call process error", e));
            return response;
        }
    }

    public synchronized void addProvider(Invoker<?> provider) {
        String serviceKey = FrameworkUtil.getServiceKey(provider.getUrl());
        if (providers.containsKey(serviceKey)) {
            throw new FrameworkException("provider alread exist: " + serviceKey);
        }

        providers.put(serviceKey, provider);

        // 获取该service暴露的方法数：
        List<Method> methods = ReflectUtil.getPublicMethod(provider.getInterface());
        CompressRpcCodec.putMethodSign(provider, methods);// 对所有接口方法生成方法签名。适配方法签名压缩调用方式。

        int publicMethodCount = methods.size();
        methodCounter.addAndGet(publicMethodCount);

        LOGGER.info("RequestRouter addProvider: url=" + provider.getUrl() + " all_public_method_count=" + methodCounter.get());
    }

    public synchronized void removeProvider(Invoker<?> provider) {
        String serviceKey = FrameworkUtil.getServiceKey(provider.getUrl());

        providers.remove(serviceKey);
        List<Method> methods = ReflectUtil.getPublicMethod(provider.getInterface());
        int publicMethodCount = methods.size();
        methodCounter.getAndSet(methodCounter.get() - publicMethodCount);

        LOGGER.info("RequestRouter removeProvider: url=" + provider.getUrl() + " all_public_method_count=" + methodCounter.get());
    }

    public int getPublicMethodCount() {
        return methodCounter.get();
    }
}
