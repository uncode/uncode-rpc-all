package cn.uncode.rpc.exception;

import cn.uncode.rpc.common.message.Message;

/**
 * @author : juny.ye
 */
public class SPIException extends AbstractException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
    public SPIException(Message msg) {
        super(msg);
    }
    
    public SPIException(Message msg, Throwable cause) {
        super(msg, cause);
    }
    
    public SPIException(String msg) {
        super(msg);
    }
    
    public SPIException(String msg, Throwable cause) {
        super(msg, cause);
    }

}
