package cn.uncode.rpc.core;

import cn.uncode.rpc.spi.Scope;
import cn.uncode.rpc.spi.Spi;

/**
 * 
 * protocol
 * 
 * <pre>
 * 只负责点到点的通讯
 * </pre>
 * 
 * @author fishermen
 * @version V1.0 created at: 2013-5-16
 */
@Spi(scope = Scope.SINGLETON)
public interface Protocol {
    /**
     * 暴露服务
     * 
     * @param <T>
     * @param provider
     * @param url
     * @return
     */
    <T> Exporter<T> export(Invoker<T> invoker, URL url);

    /**
     * 引用服务
     * 
     * @param <T>
     * @param clz
     * @param url
     * @param serviceUrl
     * @return
     */
    <T> Invoker<T> refer(Class<T> clz, URL url, URL serviceUrl);

    /**
     * <pre>
	 * 		1） exporter destroy
	 * 		2）caller destroy
	 * </pre>
     * 
     */
    void destroy();
}
