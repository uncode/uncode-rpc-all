package cn.uncode.rpc.transport.netty;

import java.net.InetSocketAddress;

import cn.uncode.rpc.common.message.Message;
import cn.uncode.rpc.exception.AbstractException;


public class NettyException extends AbstractException {

    private static final long serialVersionUID = 7057762354907226994L;

    private InetSocketAddress localAddress;
    private InetSocketAddress remoteAddress;

    public NettyException(InetSocketAddress localAddress, InetSocketAddress remoteAddress, String message) {
        super(message);
        this.localAddress = localAddress;
        this.remoteAddress = remoteAddress;
    }

    public NettyException(InetSocketAddress localAddress, InetSocketAddress remoteAddress, String message, Throwable cause) {
        super(message, cause);
        this.localAddress = localAddress;
        this.remoteAddress = remoteAddress;
    }
    
    public NettyException(Message msg) {
        super(msg);
    }
    
    public NettyException(Message msg, Throwable cause) {
        super(msg, cause);
    }

    public NettyException(Throwable cause) {
        super(cause);
    }
    
    public NettyException(String msg) {
        super(msg);
    }
    
    public NettyException(String msg, Throwable cause) {
        super(msg, cause);
    }

    public InetSocketAddress getLocalAddress() {
        return localAddress;
    }

    public InetSocketAddress getRemoteAddress() {
        return remoteAddress;
    }

}
