
package cn.uncode.rpc.spring;

import java.util.Set;

import org.springframework.beans.factory.xml.NamespaceHandlerSupport;

import cn.uncode.rpc.config.BasicCallerConfig;
import cn.uncode.rpc.config.BasicProviderConfig;
import cn.uncode.rpc.config.ProtocolConfig;
import cn.uncode.rpc.config.RegistryConfig;
import cn.uncode.rpc.util.ConcurrentHashSet;




public class UncodeNamespaceHandler extends NamespaceHandlerSupport {
    public final static Set<String> protocolDefineNames = new ConcurrentHashSet<String>();
    public final static Set<String> registryDefineNames = new ConcurrentHashSet<String>();
    public final static Set<String> basicProviderConfigDefineNames = new ConcurrentHashSet<String>();
    public final static Set<String> basicCallerConfigDefineNames = new ConcurrentHashSet<String>();

    @Override
    public void init() {
    	registerBeanDefinitionParser("basicProvider", new UncodeBeanDefinitionParser(BasicProviderConfig.class, true));
    	registerBeanDefinitionParser("provider", new UncodeBeanDefinitionParser(ProviderConfigBean.class, true));
    	registerBeanDefinitionParser("basicCaller", new UncodeBeanDefinitionParser(BasicCallerConfig.class, true));
    	registerBeanDefinitionParser("caller", new UncodeBeanDefinitionParser(CallerConfigBean.class, true));
    	registerBeanDefinitionParser("annotation", new UncodeBeanDefinitionParser(AnnotationBean.class, true));
    	registerBeanDefinitionParser("registry", new UncodeBeanDefinitionParser(RegistryConfig.class, true));
        registerBeanDefinitionParser("spi", new UncodeBeanDefinitionParser(SpiConfigBean.class, true));
        registerBeanDefinitionParser("protocol", new UncodeBeanDefinitionParser(ProtocolConfig.class, true));
    }
}
