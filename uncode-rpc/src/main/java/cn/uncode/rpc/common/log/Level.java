package cn.uncode.rpc.common.log;

/**
 * Level
 * 
 */
public enum Level {

    /**
     * ALL
     */
	ALL,
	
	/**
     * TRACE
     */
	TRACE,
	
	/**
     * DEBUG
     */
	DEBUG,
	
	/**
     * INFO
     */
	INFO,
	
	/**
     * WARN
     */
	WARN,
	
	/**
     * ERROR
     */
	ERROR,

	/**
     * OFF
     */
	OFF

}