package cn.uncode.rpc.core;

/**
 * node manage interface
 * 节点抽象
 * 
 */
public interface Node {

    void init();

    void destroy();

    boolean isAvailable();

    String desc();

    URL getUrl();
}
