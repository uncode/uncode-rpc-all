package cn.uncode.rpc.spring;


import java.util.List;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryAware;
import org.springframework.beans.factory.BeanNameAware;
import org.springframework.beans.factory.InitializingBean;

import cn.uncode.rpc.config.BasicCallerConfig;
import cn.uncode.rpc.config.ProtocolConfig;
import cn.uncode.rpc.config.RegistryConfig;
import cn.uncode.rpc.util.SpringBeanUtil;

/**
 * @author fld
 *         <p>
 *         Created by fld on 16/5/13.
 */
public class BasicCallerConfigBean extends BasicCallerConfig implements BeanNameAware, InitializingBean, BeanFactoryAware {

	private static final long serialVersionUID = 318156901969377740L;
	
	private String protocolNames;
    private String registryNames;
    private BeanFactory beanFactory;

    @Override
    public void setBeanName(String name) {
        setId(name);
        UncodeNamespaceHandler.basicCallerConfigDefineNames.add(name);
    }

    public void setProtocol(String protocolNames) {
        this.protocolNames = protocolNames;
    }

    public void setRegistry(String registryNames) {
        this.registryNames = registryNames;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        setRegistries(extractRegistries(registryNames, beanFactory));
        setProtocols(extractProtocols(protocolNames, beanFactory));
    }

    public List<ProtocolConfig> extractProtocols(String protocols, BeanFactory beanFactory) {
        if (protocols != null && protocols.length() > 0) {
            List<ProtocolConfig> protocolConfigList = SpringBeanUtil.getMultiBeans(beanFactory, protocols,
                    SpringBeanUtil.COMMA_SPLIT_PATTERN, ProtocolConfig.class);
            return protocolConfigList;
        } else {
            return null;
        }
    }

    public List<RegistryConfig> extractRegistries(String registries, BeanFactory beanFactory) {
        if (registries != null && registries.length() > 0) {
            List<RegistryConfig> registryConfigList = SpringBeanUtil.getMultiBeans(beanFactory, registries,
                    SpringBeanUtil.COMMA_SPLIT_PATTERN, RegistryConfig.class);
            return registryConfigList;
        } else {
            return null;
        }
    }


    @Override
    public void setBeanFactory(BeanFactory beanFactory) throws BeansException {
        this.beanFactory = beanFactory;
    }

    public void setCheck(boolean value) {
        setCheck(String.valueOf(value));
    }

    public void setAccessLog(boolean value) {
        setAccessLog(String.valueOf(value));
    }
}
