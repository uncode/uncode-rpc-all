

package cn.uncode.rpc.filter;

import cn.uncode.rpc.core.Invoker;
import cn.uncode.rpc.core.Request;
import cn.uncode.rpc.core.Response;
import cn.uncode.rpc.spi.Spi;

/**
 * 
 * filter with transport.
 */
@Spi
public interface Filter {

    Response filter(Invoker<?> invoker, Request request);
    
}
