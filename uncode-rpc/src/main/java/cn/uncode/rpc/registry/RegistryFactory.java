
package cn.uncode.rpc.registry;

import cn.uncode.rpc.core.URL;
import cn.uncode.rpc.spi.Scope;
import cn.uncode.rpc.spi.Spi;

/**
 * 
 * To create registry
 *
 */
@Spi(scope = Scope.SINGLETON)
public interface RegistryFactory {

    Registry getRegistry(URL url);
    
}
