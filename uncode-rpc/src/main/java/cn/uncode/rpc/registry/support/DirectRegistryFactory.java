package cn.uncode.rpc.registry.support;

import cn.uncode.rpc.core.URL;
import cn.uncode.rpc.registry.Registry;
import cn.uncode.rpc.spi.SpiMeta;

/**
 * Created by axb on 16/6/12.
 */
@SpiMeta(name = "direct")
public class DirectRegistryFactory extends AbstractRegistryFactory {

    @Override
    protected Registry createRegistry(URL url) {
        return new DirectRegistry(url);
    }


}
