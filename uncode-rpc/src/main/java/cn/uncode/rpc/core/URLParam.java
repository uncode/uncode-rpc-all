package cn.uncode.rpc.core;

import cn.uncode.rpc.common.CommonConstant;

/**
 * @author maijunsheng
 * @version 创建时间：2013-5-21
 *
 */
public enum URLParam {
	
    /** version **/
    VERSION("version", CommonConstant.DEFAULT_VERSION),
    /** request timeout **/
    REQUEST_TIMEOUT("requestTimeout", 200),
    /** request id from http interface **/
    REQUEST_ID_FROM_CLIENT("requestIdFromClient", 0),
    /** connect timeout **/
    CONNECT_TIMEOUT("connectTimeout", 1000),
    /** service min worker threads **/
    MIN_WORKER_THREAD("minWorkerThread", 20),
    /** service max worker threads **/
    MAX_WORDER_THREAD("maxWorkerThread", 200),
    /** pool min conn number **/
    MIN_CLIENT_CONNECTION("minClientConnection", 2),
    /** pool max conn number **/
    MAX_CLIENT_CONNECTION("maxClientConnection", 10),
    /** pool max conn number **/
    MAX_CONTENT_LENGTH("maxContentLength", 10 * 1024 * 1024),
    /** max server conn (all clients conn) **/
    MAX_SERVER_CONNECTION("maxServerConnection", 100000),
    /** pool conn manger stragy **/
    POOL_LIFO("poolLifo", true),

    LAZY_INIT("lazyInit", false),
    /** multi referer share the same channel **/
    SHARE_CHANNEL("shareChannel", false),

    /************************** SPI start ******************************/

    /** serialize **/
    SERIALIZE("serialization", "hessian2"),
    /** codec **/
    CODEC("codec", "motan"),
    /** endpointFactory **/
    ENDPOINT_FACTORY("endpointFactory", "uncode"),
    /** heartbeatFactory **/
    HEARTBEAT_FACTORY("heartbeatFactory", "uncode"),
    /** switcherService **/
    switcherService("switcherService", "localSwitcherService"),

    /************************** SPI end ******************************/

    GROUP("group", "default_rpc"), 
    CLIENT_GROUP("clientGroup", "default_rpc"), 
    ACCESS_LOG("accessLog", false),

    // 0为不做并发限制
    actives("actives", 0),

    REFRESH_TIMESTAMP("refreshTimestamp", 0), 
    NODE_TYPE("nodeType", CommonConstant.NODE_TYPE_PROVIDER),

    // 格式为protocol:port
    export("export", ""),
    EMBED("embed", ""),

    REGISTRY_RETRY_PERIOD("registryRetryPeriod", 30 * CommonConstant.SECOND_MILLS),
    /* 注册中心不可用节点剔除方式 */
//    excise("excise", Excise.excise_dynamic.getName()), 
    CLUSTER("cluster", CommonConstant.DEFAULT_VALUE), 
    LOADBALANCE("loadbalance", "activeWeight"),
    HA_STRATEGY("haStrategy", "failover"), 
    PROTOCOL("protocol", CommonConstant.PROTOCOL_UNCODE), 
    PATH("path", ""), 
    HOST("host", ""), 
    port("port", 0), 
    iothreads("iothreads", Runtime.getRuntime().availableProcessors() + 1), 
    WORKER_QUEUE_SIZE("workerQueueSize", 0), 
    acceptConnections("acceptConnections", 0), 
    PROXY("proxy", CommonConstant.PROXY_JDK), 
    FILTER("filter", ""),

    USEGZ("usegz", false), // 是否开启gzip压缩
    MING_SIZE("mingzSize", 1000), // 进行gz压缩的最小数据大小。超过此阈值才进行gz压缩


    APPLICATION("application", CommonConstant.FRAMEWORK_NAME), 
    MODULE("module", CommonConstant.FRAMEWORK_NAME),

    RETRIES("retries", 0), 
    ASYNC("async", false), 
    mock("mock", "false"), 
    mean("mean", "2"), 
    p90("p90", "4"), 
    p99("p99", "10"), 
    p999("p999", "70"), 
    errorRate("errorRate", "0.01"), 
    CHECK("check", "true"), 
    DIRECT_URL("directUrl", ""),
//    registrySessionTimeout("registrySessionTimeout", 1 * MotanConstants.MINUTE_MILLS),

    register("register", true), 
    subscribe("subscribe", true), 
    THROW_EXCEPTION("throwException", "true"),

    localServiceAddress("localServiceAddress", ""),

    // 切换group时，各个group的权重比。默认无权重
	WEIGHTS("weights", "");

    private String name;
    private String value;
    private long longValue;
    private int intValue;
    private boolean boolValue;

    private URLParam(String name, String value) {
        this.name = name;
        this.value = value;
    }

    private URLParam(String name, long longValue) {
        this.name = name;
        this.value = String.valueOf(longValue);
        this.longValue = longValue;
    }

    private URLParam(String name, int intValue) {
        this.name = name;
        this.value = String.valueOf(intValue);
        this.intValue = intValue;
    }

    private URLParam(String name, boolean boolValue) {
        this.name = name;
        this.value = String.valueOf(boolValue);
        this.boolValue = boolValue;
    }

    public String getName() {
        return name;
    }

    public String getValue() {
        return value;
    }

    public int getIntValue() {
        return intValue;
    }

    public long getLongValue() {
        return longValue;
    }

    public boolean getBooleanValue() {
        return boolValue;
    }

}
