package cn.uncode.rpc.spring;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryAware;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.ListableBeanFactory;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;

import cn.uncode.rpc.common.CommonConstant;
import cn.uncode.rpc.config.BasicProviderConfig;
import cn.uncode.rpc.config.ProtocolConfig;
import cn.uncode.rpc.config.ProviderConfig;
import cn.uncode.rpc.config.RegistryConfig;
import cn.uncode.rpc.exception.FrameworkException;
import cn.uncode.rpc.util.CollectionUtil;
import cn.uncode.rpc.util.ConfigUtil;
import cn.uncode.rpc.util.FrameworkUtil;



public class ProviderConfigBean<T> extends ProviderConfig<T>
        implements
        BeanPostProcessor,
        BeanFactoryAware,
        InitializingBean,
        DisposableBean,
        ApplicationListener<ContextRefreshedEvent> {

    private static final long serialVersionUID = -7247592395983804440L;

    private transient BeanFactory beanFactory;

    @Override
    public void destroy() throws Exception {
        unexport();
    }

    @Override
    public void afterPropertiesSet() throws Exception {

        // 注意:basicConfig需要首先配置，因为其他可能会依赖于basicConfig的配置
        checkAndConfigBasicConfig();
        checkAndConfigExport();
        checkAndConfigRegistry();

        // 等spring初始化完毕后，再export服务
        // export();
    }

    @Override
    public void setBeanFactory(BeanFactory beanFactory) throws BeansException {
        this.beanFactory = beanFactory;
    }

    // 为了让serviceBean最早加载
    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
        return bean;
    }

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        return bean;
    }

    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {
        if (!getExported().get()) {
            export();
        }
    }

    /**
     * 检查并配置basicConfig
     */
    private void checkAndConfigBasicConfig() {
        if (getBasicServiceConfig() == null) {
            if (UncodeNamespaceHandler.basicProviderConfigDefineNames.size() == 0) {
				if (beanFactory instanceof ListableBeanFactory) {
					ListableBeanFactory listableBeanFactory = (ListableBeanFactory) beanFactory;
					String[] basicServiceConfigNames = listableBeanFactory
							.getBeanNamesForType(BasicProviderConfig.class);
					UncodeNamespaceHandler.basicProviderConfigDefineNames.addAll(Arrays.asList(basicServiceConfigNames));
				}
            }
            for (String name : UncodeNamespaceHandler.basicProviderConfigDefineNames) {
            	BasicProviderConfig biConfig = beanFactory.getBean(name, BasicProviderConfig.class);
                if (biConfig == null) {
                    continue;
                }
                if (UncodeNamespaceHandler.basicProviderConfigDefineNames.size() == 1) {
                    setBasicServiceConfig(biConfig);
                } else if (biConfig.isDefault() != null && biConfig.isDefault().booleanValue()) {
                    setBasicServiceConfig(biConfig);
                }
            }
        }
    }

    /**
     * 检查是否已经装配export，如果没有则到basicConfig查找
     */
    private void checkAndConfigExport() {
        if (StringUtils.isBlank(getExport()) && getBasicServiceConfig() != null
                && !StringUtils.isBlank(getBasicServiceConfig().getExport())) {
            setExport(getBasicServiceConfig().getExport());
            if (getBasicServiceConfig().getProtocols() != null) {
                setProtocols(new ArrayList<ProtocolConfig>(getBasicServiceConfig().getProtocols()));
            }
        }

        if (CollectionUtil.isEmpty(getProtocols()) && StringUtils.isNotEmpty(getExport())) {
            Map<String, Integer> exportMap = ConfigUtil.parseExport(export);
            if (!exportMap.isEmpty()) {
                List<ProtocolConfig> protos = new ArrayList<ProtocolConfig>();
                for (String p : exportMap.keySet()) {
                    ProtocolConfig proto = null;
                    try {
                        proto = beanFactory.getBean(p, ProtocolConfig.class);
                    } catch (NoSuchBeanDefinitionException e) {}
                    if (proto == null) {
                        if (CommonConstant.PROTOCOL_UNCODE.equals(p)) {
                            proto = FrameworkUtil.getDefaultProtocolConfig();
                        } else {
                            throw new FrameworkException(String.format("cann't find %s ProtocolConfig bean! export:%s", p, export));
                        }
                    }

                    protos.add(proto);
                }
                setProtocols(protos);
            }
        }
        if (StringUtils.isEmpty(getExport()) || CollectionUtil.isEmpty(getProtocols())) {
            throw new FrameworkException(String.format("%s ServiceConfig must config right export value!", getInterface().getName()));
        }
    }

    /**
     * 检查并配置registry
     */
    private void checkAndConfigRegistry() {
        if (CollectionUtil.isEmpty(getRegistries()) && getBasicServiceConfig() != null
                && !CollectionUtil.isEmpty(getBasicServiceConfig().getRegistries())) {
            setRegistries(getBasicServiceConfig().getRegistries());
        }
        if (CollectionUtil.isEmpty(getRegistries())) {
            for (String name : UncodeNamespaceHandler.registryDefineNames) {
                RegistryConfig rc = beanFactory.getBean(name, RegistryConfig.class);
                if (rc == null) {
                    continue;
                }
                if (UncodeNamespaceHandler.registryDefineNames.size() == 1) {
                    setRegistry(rc);
                } else if (rc.isDefault() != null && rc.isDefault().booleanValue()) {
                    setRegistry(rc);
                }
            }
        }
        if (CollectionUtil.isEmpty(getRegistries())) {
            setRegistry(FrameworkUtil.getDefaultRegistryConfig());
        }
    }

}
