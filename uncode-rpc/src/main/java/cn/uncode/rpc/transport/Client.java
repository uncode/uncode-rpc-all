
package cn.uncode.rpc.transport;

import java.net.InetSocketAddress;

import cn.uncode.rpc.core.Request;
import cn.uncode.rpc.core.Response;
import cn.uncode.rpc.core.URL;
import cn.uncode.rpc.exception.TransportException;

/**
 * 
 * 类说明
 * 
 */

public interface Client {
    
    boolean connect() throws TransportException;
    
    boolean reConnect() throws TransportException;
    
    Response send(Request request)throws TransportException;
    
    void close()throws TransportException;
    
    boolean isClosed();
    
    boolean isAvailable();
    
    URL getUrl();
    
    
    /**
     * async send request.
     *
     * @param request
     */
    void heartbeat(Request request);
    
    
    InetSocketAddress getLocalAddress();

    InetSocketAddress getRemoteAddress(); 
    
}
