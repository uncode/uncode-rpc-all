package cn.uncode.rpc.util;

import java.util.Collection;

/**
 * 
 * Utils for collections.
 *
 * @author fishermen
 * @version V1.0 created at: 2013-6-7
 */

public class CollectionUtil {

    @SuppressWarnings("rawtypes")
    public static boolean isEmpty(Collection collection) {
        return collection == null || collection.size() == 0;
    }

}
