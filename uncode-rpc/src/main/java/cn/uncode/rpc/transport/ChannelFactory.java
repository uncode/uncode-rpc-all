package cn.uncode.rpc.transport;

import cn.uncode.rpc.core.URL;
import cn.uncode.rpc.spi.Scope;
import cn.uncode.rpc.spi.Spi;

/**
 * @author maijunsheng
 * @version 创建时间：2013-6-5
 * 
 */
@Spi(scope = Scope.SINGLETON)
public interface ChannelFactory {

    /**
     * create remote server
     * 
     * @param url
     * @param messageHandler
     * @return
     */
    Server createServer(URL url, RequestHandler requestHandler);

    /**
     * create remote client
     * 
     * @param url
     * @return
     */
    Client createClient(URL url);

    /**
     * safe release server
     * 
     * @param server
     * @param url
     */
    void safeReleaseResource(Server server, URL url);

    /**
     * safe release client
     * 
     * @param client
     * @param url
     */
    void safeReleaseResource(Client client, URL url);

}
