package cn.uncode.rpc.spring;

import java.util.Collections;
import java.util.List;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryAware;
import org.springframework.beans.factory.BeanNameAware;
import org.springframework.beans.factory.InitializingBean;

import cn.uncode.rpc.config.BasicProviderConfig;
import cn.uncode.rpc.config.ProtocolConfig;
import cn.uncode.rpc.config.RegistryConfig;
import cn.uncode.rpc.util.ConfigUtil;
import cn.uncode.rpc.util.SpringBeanUtil;




/**
 * @author fld
 *         <p>
 *         Created by fld on 16/5/13.
 */
public class BasicProviderConfigBean extends BasicProviderConfig implements BeanNameAware, InitializingBean, BeanFactoryAware {

    /**
	 * 
	 */
	private static final long serialVersionUID = 8903577914909380446L;


	private String registryNames;


    BeanFactory beanFactory;

    @Override
    public void setBeanName(String name) {
        setId(name);

        UncodeNamespaceHandler.basicProviderConfigDefineNames.add(name);
    }

    @Override
    public void setBeanFactory(BeanFactory beanFactory) throws BeansException {
        this.beanFactory = beanFactory;
    }


    @Override
    public void afterPropertiesSet() throws Exception {
        String protocol = ConfigUtil.extractProtocols(getExport());
        setRegistries(extractRegistries(registryNames, beanFactory));
        setProtocols(extractProtocols(protocol, beanFactory));
    }

    public List<ProtocolConfig> extractProtocols(String protocols, BeanFactory beanFactory) {
        if (protocols != null && protocols.length() > 0) {
            List<ProtocolConfig> protocolConfigList = SpringBeanUtil.getMultiBeans(beanFactory, protocols,
                    SpringBeanUtil.COMMA_SPLIT_PATTERN, ProtocolConfig.class);

            return protocolConfigList;
        } else {
            return null;
        }
    }

    public List<RegistryConfig> extractRegistries(String registries, BeanFactory beanFactory) {
        if (registries != null && registries.length() > 0) {
            if (!registries.contains(",")) {
                RegistryConfig registryConfig = beanFactory.getBean(registries, RegistryConfig.class);
                return Collections.singletonList(registryConfig);
            } else {
                List<RegistryConfig> registryConfigList = SpringBeanUtil.getMultiBeans(beanFactory, registries,
                        SpringBeanUtil.COMMA_SPLIT_PATTERN, RegistryConfig.class);
                return registryConfigList;
            }
        } else {
            return null;
        }
    }


    public void setRegistry(String registryNames) {
        this.registryNames = registryNames;
    }

    public void setCheck(boolean value) {
        setCheck(String.valueOf(value));
    }

    public void setAccessLog(boolean value) {
        setAccessLog(String.valueOf(value));
    }
}
