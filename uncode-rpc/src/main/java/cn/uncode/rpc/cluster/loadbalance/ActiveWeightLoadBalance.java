package cn.uncode.rpc.cluster.loadbalance;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Random;

import cn.uncode.rpc.core.Invoker;
import cn.uncode.rpc.core.Request;
import cn.uncode.rpc.spi.SpiMeta;


/**
 * "低并发优化" 负载均衡
 * 
 * <pre>
 * 		1） 低并发度优先： referer的某时刻的call数越小优先级越高 
 * 
 * 		2） 低并发referer获取策略：
 * 				由于Referer List可能很多，比如上百台，如果每次都要从这上百个Referer或者最低并发的几个，性能有些损耗，
 * 				因此 random.nextInt(list.size()) 获取一个起始的index，然后获取最多不超过MAX_REFERER_COUNT的
 * 				状态是isAvailable的referer进行判断activeCount.
 * </pre>
 * 
 * @author maijunsheng
 * @version 创建时间：2013-6-14
 * 
 */
@SpiMeta(name = "activeWeight")
public class ActiveWeightLoadBalance<T> extends AbstractLoadBalance<T> {
	
    private static Random random = new Random();

    @Override
    protected Invoker<T> doSelect(Request request) {
        List<Invoker<T>> invokers = getInvokers();

        int invokerSize = invokers.size();
        int startIndex = random.nextInt(invokerSize);
        int currentCursor = 0;
        int currentAvailableCursor = 0;

        Invoker<T> referer = null;

        while (currentAvailableCursor < MAX_CALLER_COUNT && currentCursor < invokerSize) {
            Invoker<T> temp = invokers.get((startIndex + currentCursor) % invokerSize);
            currentCursor++;

            if (!temp.isAvailable()) {
                continue;
            }

            currentAvailableCursor++;

            if (referer == null) {
                referer = temp;
            } else {
                if (compare(referer, temp) > 0) {
                    referer = temp;
                }
            }
        }

        return referer;
    }

    @Override
    protected void doSelectToHolder(Request request, List<Invoker<T>> refersHolder) {
        List<Invoker<T>> invokers = getInvokers();

        int invokerSize = invokers.size();
        int startIndex = random.nextInt(invokerSize);
        int currentCursor = 0;
        int currentAvailableCursor = 0;

        while (currentAvailableCursor < MAX_CALLER_COUNT && currentCursor < invokerSize) {
            Invoker<T> temp = invokers.get((startIndex + currentCursor) % invokerSize);
            currentCursor++;

            if (!temp.isAvailable()) {
                continue;
            }

            currentAvailableCursor++;

            refersHolder.add(temp);
        }

        Collections.sort(refersHolder, new LowActivePriorityComparator<T>());
    }

    private int compare(Invoker<T> invoker1, Invoker<T> invoker2) {
        return invoker1.activeInvokeCount() - invoker2.activeInvokeCount();
    }

    static class LowActivePriorityComparator<T> implements Comparator<Invoker<T>> {
        @Override
        public int compare(Invoker<T> invoker1, Invoker<T> invoker2) {
            return invoker1.activeInvokeCount() - invoker2.activeInvokeCount();
        }
    }

}
