package cn.uncode.rpc.exception;

import java.net.InetSocketAddress;

import cn.uncode.rpc.common.message.Message;


public class TransportException extends AbstractException {

    private static final long serialVersionUID = 7057762354907226994L;

    private InetSocketAddress localAddress;
    private InetSocketAddress remoteAddress;

    public TransportException(InetSocketAddress localAddress, InetSocketAddress remoteAddress, String message) {
        super(message);
        this.localAddress = localAddress;
        this.remoteAddress = remoteAddress;
    }

    public TransportException(InetSocketAddress localAddress, InetSocketAddress remoteAddress, String message, Throwable cause) {
        super(message, cause);
        this.localAddress = localAddress;
        this.remoteAddress = remoteAddress;
    }
    
    public TransportException(Message msg) {
        super(msg);
    }
    
    public TransportException(Message msg, Throwable cause) {
        super(msg, cause);
    }

    public TransportException(Throwable cause) {
        super(cause);
    }
    
    public TransportException(String msg) {
        super(msg);
    }
    
    public TransportException(String msg, Throwable cause) {
        super(msg, cause);
    }

    public InetSocketAddress getLocalAddress() {
        return localAddress;
    }

    public InetSocketAddress getRemoteAddress() {
        return remoteAddress;
    }

}
