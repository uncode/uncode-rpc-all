package cn.uncode.rpc.demo.client.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.uncode.rpc.demo.api.DemoService;
import cn.uncode.rpc.spring.annotation.Caller;

@Service
public class HelloImpl implements Hello {
	
//	@Caller(interfaceClass=DemoService.class, directUrl="192.168.1.61:8001", protocol="demo")
	@Autowired
    private DemoService demoService;

	@Override
	public String say() {
		return demoService.hello("==>client");
	}

}
