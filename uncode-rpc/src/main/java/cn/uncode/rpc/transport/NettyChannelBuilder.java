package cn.uncode.rpc.transport;

import cn.uncode.rpc.core.URL;
import cn.uncode.rpc.spi.Scope;
import cn.uncode.rpc.spi.Spi;
import cn.uncode.rpc.util.StandardThreadExecutor;
import io.netty.channel.ChannelPipeline;

@Spi(scope = Scope.SINGLETON)
public interface NettyChannelBuilder {
	
	public static final int MESSAGE_LENGTH = 4;

    void buildServer(StandardThreadExecutor standardThreadExecutor, RequestHandler requestHandler, ChannelPipeline pipeline);
    
    
    void buildClient(URL url, ChannelPipeline pipeline);

}
