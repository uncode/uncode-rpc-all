package cn.uncode.rpc.spring;

import org.springframework.beans.factory.InitializingBean;

import cn.uncode.rpc.config.SpiConfig;
import cn.uncode.rpc.spi.ExtensionLoader;




public class SpiConfigBean<T> extends SpiConfig<T> implements InitializingBean {

    @Override
    public void afterPropertiesSet() throws Exception {
        ExtensionLoader.getExtensionLoader(getInterfaceClass()).addExtensionClass(getSpiClass());
    }

}
