package cn.uncode.rpc.util;

import cn.uncode.rpc.exception.AbstractException;
import cn.uncode.rpc.exception.FrameworkException;

public class ExceptionUtil {

    /**
     * 判定是否是业务方的逻辑抛出的异常
     * 
     * <pre>
	 * 		true: 来自业务方的异常
	 * 		false: 来自框架本身的异常
	 * </pre>
     * 
     * @param e
     * @return
     */
    public static boolean isBizException(Exception e) {
        return e instanceof FrameworkException;
    }


    /**
     * 是否框架包装过的异常
     */
    public static boolean isRPCException(Exception e) {
        return e instanceof AbstractException;
    }
}
