package cn.uncode.rpc.cluster;

import java.util.List;

import cn.uncode.rpc.core.Invoker;
import cn.uncode.rpc.core.URL;
import cn.uncode.rpc.spi.Scope;
import cn.uncode.rpc.spi.Spi;


/**
 * 
 * Cluster is a service broker, used to
 * 
 */

@Spi(scope = Scope.PROTOTYPE)
public interface Cluster<T> extends Invoker<T> {

    void setUrl(URL url);

    void setLoadBalance(LoadBalance<T> loadBalance);

    void setHaStrategy(HaStrategy<T> haStrategy);

    void onRefresh(List<Invoker<T>> invokers);

    List<Invoker<T>> getInvokers();

    LoadBalance<T> getLoadBalance();
    
}
