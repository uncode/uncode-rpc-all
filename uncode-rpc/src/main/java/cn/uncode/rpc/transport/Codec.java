package cn.uncode.rpc.transport;

import java.io.IOException;

import cn.uncode.rpc.spi.Scope;
import cn.uncode.rpc.spi.Spi;



@Spi(scope=Scope.PROTOTYPE)
public interface Codec {

	byte[] encode(Channel channel, Object message) throws IOException;

	/**
	 * 
	 * @param channel
	 * @param remoteIp 用来在server端decode request时能获取到client的ip。
	 * @param buffer
	 * @return
	 * @throws IOException
	 */
	Object decode(Channel channel, String remoteIp, byte[] buffer) throws IOException;

}
