package cn.uncode.rpc.transport.support;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.OutputStream;

import cn.uncode.rpc.exception.FrameworkException;
import cn.uncode.rpc.serialize.Serialization;
import cn.uncode.rpc.transport.Codec;


/**
 * 编解码
 * @author wj.ye
 *
 */
public abstract class AbstractCodec implements Codec {
	
    protected void serialize(ObjectOutput output, Object message, Serialization serialize) throws IOException {
        if (message == null) {
            output.writeObject(null);
            return;
        }

        output.writeObject(serialize.serialize(message));
    }

    protected Object deserialize(byte[] value, Class<?> type, Serialization serialize) throws IOException {
        if (value == null) {
            return null;
        }

        return serialize.deserialize(value, type);
    }

    public ObjectOutput createOutput(OutputStream outputStream) {
        try {
            return new ObjectOutputStream(outputStream);
        } catch (Exception e) {
            throw new FrameworkException(this.getClass().getSimpleName() + " createOutput error");
        }
    }

    public ObjectInput createInput(InputStream in) {
        try {
            return new ObjectInputStream(in);
        } catch (Exception e) {
            throw new FrameworkException(this.getClass().getSimpleName() + " createInput error");
        }
    }

}
