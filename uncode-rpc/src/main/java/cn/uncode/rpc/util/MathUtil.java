package cn.uncode.rpc.util;

import cn.uncode.rpc.common.log.Logger;
import cn.uncode.rpc.common.log.LoggerFactory;

public class MathUtil {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(MathUtil.class);

    public static int parseInt(String intStr, int defaultValue) {
        try {
            return Integer.parseInt(intStr);
        } catch (NumberFormatException e) {
        	LOGGER.debug("ParseInt false, for malformed intStr:" + intStr);
            return defaultValue;
        }
    }
}
