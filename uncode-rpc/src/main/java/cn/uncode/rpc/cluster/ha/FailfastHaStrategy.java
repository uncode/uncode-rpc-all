package cn.uncode.rpc.cluster.ha;

import cn.uncode.rpc.cluster.LoadBalance;
import cn.uncode.rpc.core.Invoker;
import cn.uncode.rpc.core.Request;
import cn.uncode.rpc.core.Response;
import cn.uncode.rpc.spi.SpiMeta;

/**
 * 
 * Fail fast ha strategy.
 */
@SpiMeta(name = "failfast")
public class FailfastHaStrategy<T> extends AbstractHaStrategy<T> {

    @Override
    public Response call(Request request, LoadBalance<T> loadBalance) {
    	Invoker<T> refer = loadBalance.select(request);
        return refer.invoke(request);
    }
}
