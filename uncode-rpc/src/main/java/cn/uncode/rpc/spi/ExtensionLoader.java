package cn.uncode.rpc.spi;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Constructor;
import java.lang.reflect.Modifier;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import cn.uncode.rpc.common.log.Logger;
import cn.uncode.rpc.common.log.LoggerFactory;
import cn.uncode.rpc.common.message.Messages;
import cn.uncode.rpc.exception.SPIException;


public class ExtensionLoader<T> {
	
	private static final String DEFAULT_CHARACTER = "utf-8";
	
	private static final Logger LOGGER = LoggerFactory.getLogger(ExtensionLoader.class);
	
	private static ConcurrentMap<Class<?>, ExtensionLoader<?>> SPI_LOADERS = new ConcurrentHashMap<Class<?>, ExtensionLoader<?>>();
	
	
    private ConcurrentMap<String, T> singletonInstances = null;
    private ConcurrentMap<String, Class<T>> extensionClasses = null;
	
    private Class<T> type;
    private volatile boolean init = false;
    // spi path prefix
    private static final String PREFIX = "META-INF/services/";
    private ClassLoader classLoader;
    
    private ExtensionLoader(Class<T> type) {
        this(type, Thread.currentThread().getContextClassLoader());
    }
    
    private ExtensionLoader(Class<T> type, ClassLoader classLoader) {
        this.type = type;
        this.classLoader = classLoader;
    }
    
    //***************************************
    // public
    //***************************************
    
    @SuppressWarnings("unchecked")
    public static <T> ExtensionLoader<T> getExtensionLoader(Class<T> type) {
    	if(SPI_LOADERS == null){
    		SPI_LOADERS = new ConcurrentHashMap<Class<?>, ExtensionLoader<?>>();
    	}
    	
        checkInterfaceType(type);
        ExtensionLoader<T> loader = (ExtensionLoader<T>) SPI_LOADERS.get(type);

        if (loader == null) {
            loader = initExtensionLoader(type);
        }
        return loader;
    }

    @SuppressWarnings("unchecked")
    public static synchronized <T> ExtensionLoader<T> initExtensionLoader(Class<T> type) {
    	
    	ExtensionLoader<T> loader = (ExtensionLoader<T>) SPI_LOADERS.get(type);
        if (loader == null) {
            loader = new ExtensionLoader<T>(type);
            SPI_LOADERS.putIfAbsent(type, loader);
            loader = (ExtensionLoader<T>) SPI_LOADERS.get(type);
        }

        return loader;
    }
    
    public List<T> getExtensions(String key){
    	checkInit();
    	
    	if (extensionClasses.size() == 0) {
            return Collections.emptyList();
        }
    	
    	// 如果只有一个实现，直接返回
        List<T> exts = new ArrayList<T>(extensionClasses.size());

        // 多个实现，按优先级排序返回
        for (Map.Entry<String, Class<T>> entry : extensionClasses.entrySet()) {
        	exts.add(getExtension(entry.getKey()));
        }
        Collections.sort(exts, new SpiMetaComparator<T>());
        
        return exts;
    }
    
    public T getExtension(String name) {
        checkInit();

        if (name == null) {
            return null;
        }

        try {
            Spi spi = type.getAnnotation(Spi.class);

            if (spi.scope() == Scope.SINGLETON) {
                return getSingletonInstance(name);
            } else {
                Class<T> clz = extensionClasses.get(name);

                if (clz == null) {
                    return null;
                }

                return clz.newInstance();
            }
        } catch (Exception e) {
        	throwSPIException(type, "Error when getExtension " + name + "," + e.getMessage());
        }

        return null;
    }
    
    
    public Class<T> getExtensionClass(String name) {
        checkInit();
        return extensionClasses.get(name);
    }
    
    public void addExtensionClass(Class<T> clz) {
        if (clz == null) {
            return;
        }

        checkInit();

        checkExtensionType(clz);

        String spiName = getSpiName(clz);

        synchronized (extensionClasses) {
            if (extensionClasses.containsKey(spiName)) {
            	throwSPIException(clz, ":Error spiName already exist " + spiName);
            } else {
                extensionClasses.put(spiName, clz);
            }
        }
    }
    
    
    
    
    //***************************************
    // private
    //***************************************
    
    private void checkInit(){
    	if(!init){
    		loadExtensionClasses();
    	}
    }
    
    private T getSingletonInstance(String name) throws InstantiationException, IllegalAccessException {
        T obj = singletonInstances.get(name);

        if (obj != null) {
            return obj;
        }

        Class<T> clz = extensionClasses.get(name);

        if (clz == null) {
            return null;
        }

        synchronized (singletonInstances) {
            obj = singletonInstances.get(name);
            if (obj != null) {
                return obj;
            }

            obj = clz.newInstance();
            singletonInstances.put(name, obj);
        }

        return obj;
    }

    private synchronized void loadExtensionClasses() {
        if (init) {
            return;
        }

        extensionClasses = loadExtensionClasses(PREFIX);
        singletonInstances = new ConcurrentHashMap<String, T>();

        init = true;
    }
    
    
    private ConcurrentMap<String, Class<T>> loadExtensionClasses(String prefix) {
    	String fullName = prefix + type.getName();
    	List<String> classNames = new ArrayList<String>();
    	
		try {
			Enumeration<URL> urls;
			if (classLoader == null) {
				urls = ClassLoader.getSystemResources(fullName);
			} else {
				urls = classLoader.getResources(fullName);
			}

			if (urls == null || !urls.hasMoreElements()) {
				return new ConcurrentHashMap<String, Class<T>>();
			}

			while (urls.hasMoreElements()) {
				URL url = urls.nextElement();
				parseUrl(type, url, classNames);
			}

		} catch (IOException e) {
			throw new SPIException(Messages.getString("RuntimeError.LoadExtensionClasses", prefix, type.getClass().toString()), e);
		}
    	
    	return loadClass(classNames);
    }
    
    
    @SuppressWarnings({"unchecked" })
	private ConcurrentMap<String, Class<T>> loadClass(List<String> classNames) {
    	ConcurrentMap<String, Class<T>> map = new ConcurrentHashMap<String, Class<T>>();
    	for(String className : classNames){
    		try {
				Class<T> clz;
				if(classLoader == null){
					clz = (Class<T>)Class.forName(className);
				} else {
                    clz = (Class<T>) Class.forName(className, true, classLoader);
                }
				
				checkExtensionType(clz);
				
				String spiName = getSpiName(clz);
				
				if (map.containsKey(spiName)) {
					LOGGER.error(clz + ":Error spiName already exist: " + spiName);
                } else {
                    map.put(spiName, clz);
                }
			} catch (Exception e) {
				LOGGER.error("Error load spi class: " + type, e);
			}
    	}
    	
    	return map;
    }
    
    
    /**
     * check clz
     * 
     * <pre>
	 * 		1.  is interface
	 * 		2.  is contains @Spi annotation
	 * </pre>
     * 
     * 
     * @param <T>
     * @param clz
     */
    private static <T> void checkInterfaceType(Class<T> clz) {
        if (clz == null) {
        	throwSPIException(clz, "Error extension type is null");
        }

        if (!clz.isInterface()) {
        	throwSPIException(clz, "Error extension type is not interface");
        }

        if (!isSpiType(clz)) {
        	throwSPIException(clz, "Error extension type without @Spi annotation");
        }
    }

    
    /**
     * check extension clz
     * 
     * <pre>
	 * 		1) is public class
	 * 		2) contain public constructor and has not-args constructor
	 * 		3) check extension clz instanceof Type.class
	 * </pre>
     * 
     * @param clz
     */
    private void checkExtensionType(Class<T> clz) {
    	
        checkClassPublic(clz);

        checkConstructorPublic(clz);

        checkClassInherit(clz);
    }

    private void checkClassInherit(Class<T> clz) {
        if (!type.isAssignableFrom(clz)) {
        	throwSPIException(clz, "Error is not instanceof " + type.getName());
        }
    }

    private void checkClassPublic(Class<T> clz) {
        if (!Modifier.isPublic(clz.getModifiers())) {
        	throwSPIException(clz, "Error is not a public class");
        }
    }

    private void checkConstructorPublic(Class<T> clz) {
        Constructor<?>[] constructors = clz.getConstructors();

        if (constructors == null || constructors.length == 0) {
        	throwSPIException(clz, "Error has no public no-args constructor");
        }

        for (Constructor<?> constructor : constructors) {
            if (Modifier.isPublic(constructor.getModifiers()) && constructor.getParameterTypes().length == 0) {
                return;
            }
        }

        throwSPIException(clz, "Error has no public no-args constructor");
    }

    private static boolean isSpiType(Class<?> clz) {
        return clz.isAnnotationPresent(Spi.class);
    }
    
    /**
     * 获取扩展点的名字
     * 
     * <pre>
	 * 		如果扩展类有SpiMeta的注解，那么获取对应的name，如果没有的话获取classname
	 * </pre>
     * 
     * @param clz
     * @return string
     */
    private String getSpiName(Class<?> clz) {
    	
        SpiMeta spiMeta = clz.getAnnotation(SpiMeta.class);

        String name = (spiMeta != null && !"".equals(spiMeta.name())) ? spiMeta.name() : clz.getSimpleName();

        return name;
    }
    
    
    
    private void parseUrl(Class<T> type, URL url, List<String> classNames) throws SPIException {
    	InputStream inputStream = null;
    	BufferedReader reader = null;
    	
    	try {
    		inputStream = url.openStream();
    		reader = new BufferedReader(new InputStreamReader(inputStream, DEFAULT_CHARACTER));
    		
    		String line = null;
    		int indexNumber = 0;
    		
    		while ((line = reader.readLine()) != null) {
                indexNumber++;
                parseLine(type, url, line, indexNumber, classNames);
            }
    		
		} catch (Exception e) {
			LOGGER.error(type.getName() + ": Error reading spi configuration file", e);
		} finally{
			try {
                if (reader != null) {
                    reader.close();
                }
                if (inputStream != null) {
                    inputStream.close();
                }
            } catch (IOException ie) {
            	LOGGER.error(type.getName() + ": Error reading spi configuration file", ie);
            }
		}
    }
    
    
	private void parseLine(Class<T> type, URL url, String line, int lineNumber, List<String> names)
			throws IOException, SPIException {
		
		int charIndex = line.indexOf('#');
		if(charIndex >= 0){
			line = line.substring(0, charIndex);
		}
		line = line.trim();
		if(line.length() <= 0){
			return;
		}
		
		if ((line.indexOf(' ') >= 0) || (line.indexOf('\t') >= 0)) {
			throwSPIException(type, url, lineNumber, "Illegal spi configuration-file syntax");
        }
		
		int charPT = line.codePointAt(0);
		if(!Character.isJavaIdentifierPart(charPT)){
			throwSPIException(type, url, lineNumber, "Illegal spi provider-class name: " + line);
		}
		
		for (int i = Character.charCount(charPT); i < line.length(); i += Character.charCount(charPT)) {
			charPT = line.codePointAt(i);
            if (!Character.isJavaIdentifierPart(charPT) && (charPT != '.')) {
            	throwSPIException(type, url, lineNumber, "Illegal spi provider-class name: " + line);
            }
        }
		
		if (!names.contains(line)) {
            names.add(line);
        }
	}
	
	
	private void throwSPIException(Class<T> type, URL url, int line, String msg) throws SPIException {
		throw new SPIException(type.getName() + ": " + url + ": " + line + ": " + msg);
    }
	
    private static <T> void throwSPIException(Class<T> type, String msg) {
        throw new SPIException(type.getName() + ": " + msg);
    }



}
