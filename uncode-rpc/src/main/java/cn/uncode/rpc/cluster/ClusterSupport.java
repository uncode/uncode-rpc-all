package cn.uncode.rpc.cluster;



import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;

import cn.uncode.rpc.common.CommonConstant;
import cn.uncode.rpc.common.log.Logger;
import cn.uncode.rpc.common.log.LoggerFactory;
import cn.uncode.rpc.config.AbstractConfig;
import cn.uncode.rpc.core.Invoker;
import cn.uncode.rpc.core.Protocol;
import cn.uncode.rpc.core.URL;
import cn.uncode.rpc.core.URLParam;
import cn.uncode.rpc.core.protocol.ProxyProtocol;
import cn.uncode.rpc.exception.ClusterException;
import cn.uncode.rpc.registry.NotifyListener;
import cn.uncode.rpc.registry.Registry;
import cn.uncode.rpc.registry.RegistryFactory;
import cn.uncode.rpc.spi.ExtensionLoader;
import cn.uncode.rpc.util.CollectionUtil;
import cn.uncode.rpc.util.StringTools;

/**
 * Notify cluster the referers have changed.
 *
 * @author fishermen
 * @version V1.0 created at: 2013-5-31
 */

public class ClusterSupport<T> implements NotifyListener<T> {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(ClusterSupport.class);

    private static ConcurrentHashMap<String, Protocol> protocols = new ConcurrentHashMap<String, Protocol>();
    private Cluster<T> cluster;
    private List<URL> registryUrls;
    private URL url;
    private Class<T> interfaceClass;
    private Protocol protocol;
    private ConcurrentHashMap<URL, List<Invoker<T>>> registryReferers = new ConcurrentHashMap<URL, List<Invoker<T>>>();


    public ClusterSupport(Class<T> interfaceClass, List<URL> registryUrls) {
        this.registryUrls = registryUrls;
        this.interfaceClass = interfaceClass;
        String urlStr = StringTools.urlDecode(registryUrls.get(0).getParameter(URLParam.EMBED.getName()));
        this.url = URL.valueOf(urlStr);
        protocol = getDecorateProtocol(url.getProtocol());
    }

    public void init() {

        prepareCluster();

        URL subUrl = toSubscribeUrl(url);
        for (URL ru : registryUrls) {

            String directUrlStr = ru.getParameter(URLParam.DIRECT_URL.getName());
            // 如果有directUrl，直接使用这些directUrls进行初始化，不用到注册中心discover
            if (StringUtils.isNotBlank(directUrlStr)) {
                List<URL> directUrls = parseDirectUrls(directUrlStr);
                if (!directUrls.isEmpty()) {
                    notify(ru, directUrls);
                    LOGGER.info("Use direct urls, refUrl={}, directUrls={}", url, directUrls);
                    continue;
                }
            }

            // client 注册自己，同时订阅service列表
            Registry registry = getRegistry(ru);
            registry.subscribe(subUrl, this);
        }

        boolean check = Boolean.parseBoolean(url.getParameter(URLParam.CHECK.getName(), URLParam.CHECK.getValue()));
        if (!CollectionUtil.isEmpty(cluster.getInvokers()) || !check) {
            cluster.init();
            if (CollectionUtil.isEmpty(cluster.getInvokers()) && !check) {
            	LOGGER.warn(String.format("refer:%s", this.url.getPath() + "/" + this.url.getVersion()), "No services");
            }
            return;
        }

        throw new ClusterException(String.format("ClusterSupport No service urls for the refer:%s, registries:%s", this.url.getIdentity(), registryUrls));
    }

    public void destroy() {
        URL subscribeUrl = toSubscribeUrl(url);
        for (URL ru : registryUrls) {
            try {
                Registry registry = getRegistry(ru);
                registry.unsubscribe(subscribeUrl, this);
                if (!CommonConstant.NODE_TYPE_REFERER.equals(url.getParameter(URLParam.NODE_TYPE.getName()))) {
                    registry.unregister(url);
                }
            } catch (Exception e) {
            	LOGGER.warn(String.format("Unregister or unsubscribe false for url (%s), registry= %s", url, ru.getIdentity()), e);
            }

        }
        try {
            getCluster().destroy();
        } catch (Exception e) {
        	LOGGER.warn(String.format("Exception when destroy cluster: %s", getCluster().getUrl()));
        }
    }

    protected Registry getRegistry(URL url) {
        RegistryFactory registryFactory = ExtensionLoader.getExtensionLoader(RegistryFactory.class).getExtension(url.getProtocol());
        return registryFactory.getRegistry(url);
    }

    private URL toSubscribeUrl(URL url) {
        URL subUrl = url.createCopy();
        subUrl.addParameter(URLParam.NODE_TYPE.getName(), CommonConstant.NODE_TYPE_SERVICE);
        return subUrl;
    }

    /**
     * <pre>
     * 1 notify的执行需要串行
     * 2 notify通知都是全量通知，在设入新的referer后，cluster需要把不再使用的referer进行回收，避免资源泄漏;
     * 3 如果该registry对应的referer数量为0，而没有其他可用的referers，那就忽略该次通知；
     * 4 此处对protoco进行decorator处理，当前为增加filters
     * </pre>
     */
    @Override
    public synchronized void notify(URL registryUrl, List<URL> urls) {
        if (CollectionUtil.isEmpty(urls)) {
            onRegistryEmpty(registryUrl);
            LOGGER.warn("ClusterSupport config change notify, urls is empty: registry={} service={} urls=[]", registryUrl.getUri(),
                    url.getIdentity());

            return;
        }

        LOGGER.info("ClusterSupport config change notify: registry={} service={} urls={}", registryUrl.getUri(), url.getIdentity(),
                getIdentities(urls));

        // 通知都是全量通知，在设入新的referer后，cluster内部需要把不再使用的referer进行回收，避免资源泄漏
        // ////////////////////////////////////////////////////////////////////////////////

        // 判断urls中是否包含权重信息，并通知loadbalance。
        processWeights(urls);

        List<Invoker<T>> newReferers = new ArrayList<Invoker<T>>();
        for (URL u : urls) {
            if (!u.canServe(url)) {
                continue;
            }
            Invoker<T> referer = getExistingReferer(u, registryReferers.get(registryUrl));
            if (referer == null) {
                // careful u: serverURL, refererURL的配置会被serverURL的配置覆盖
                URL refererURL = u.createCopy();
                mergeClientConfigs(refererURL);
                referer = protocol.refer(interfaceClass, refererURL, u);
            }
            if (referer != null) {
                newReferers.add(referer);
            }
        }

        if (CollectionUtil.isEmpty(newReferers)) {
            onRegistryEmpty(registryUrl);
            return;
        }

        // 此处不销毁referers，由cluster进行销毁
        registryReferers.put(registryUrl, newReferers);
        refreshCluster();
    }

    /**
     * 检查urls中的第一个url是否为权重信息。 如果是权重信息则把权重信息传递给loadbalance，并移除权重url。
     *
     * @param urls
     */
    private void processWeights(List<URL> urls) {
        if (urls != null && !urls.isEmpty()) {
            URL ruleUrl = urls.get(0);
            // 没有权重时需要传递默认值。因为可能是变更时去掉了权重
            String weights = URLParam.WEIGHTS.getValue();
            if ("rule".equalsIgnoreCase(ruleUrl.getProtocol())) {
                weights = ruleUrl.getParameter(URLParam.WEIGHTS.getName(), URLParam.WEIGHTS.getValue());
                urls.remove(0);
            }
            LOGGER.info("refresh weight. weight=" + weights);
            this.cluster.getLoadBalance().setWeightString(weights);
        }
    }

    private void onRegistryEmpty(URL excludeRegistryUrl) {
        boolean noMoreOtherRefers = registryReferers.size() == 1 && registryReferers.containsKey(excludeRegistryUrl);
        if (noMoreOtherRefers) {
        	LOGGER.warn(String.format("Ignore notify for no more referers in this cluster, registry: %s, cluster=%s",
                    excludeRegistryUrl, getUrl()));
        } else {
            registryReferers.remove(excludeRegistryUrl);
            refreshCluster();
        }
    }

    protected Protocol getDecorateProtocol(String protocolName) {
        Protocol decorateProtocol = protocols.get(protocolName);
        if (decorateProtocol == null) {
            protocols.putIfAbsent(protocolName, new ProxyProtocol(ExtensionLoader.getExtensionLoader(Protocol.class)
                    .getExtension(protocolName)));
            decorateProtocol = protocols.get(protocolName);
        }
        return decorateProtocol;
    }

    private Invoker<T> getExistingReferer(URL url, List<Invoker<T>> referers) {
        if (referers == null) {
            return null;
        }
        for (Invoker<T> r : referers) {
            if (ObjectUtils.equals(url, r.getUrl()) || ObjectUtils.equals(url, r.getUrl())) {
                return r;
            }
        }
        return null;
    }

    /**
     * refererURL的扩展参数中，除了application、module外，其他参数被client覆盖， 如果client没有则使用referer的参数
     *
     * @param refererURL
     */
    private void mergeClientConfigs(URL refererURL) {
        String application = refererURL.getParameter(URLParam.APPLICATION.getName(), URLParam.APPLICATION.getValue());
        String module = refererURL.getParameter(URLParam.MODULE.getName(), URLParam.MODULE.getValue());
        refererURL.addParameters(this.url.getParameters());

        refererURL.addParameter(URLParam.APPLICATION.getName(), application);
        refererURL.addParameter(URLParam.MODULE.getName(), module);
    }

    private void refreshCluster() {
        List<Invoker<T>> referers = new ArrayList<Invoker<T>>();
        for (List<Invoker<T>> refs : registryReferers.values()) {
            referers.addAll(refs);
        }
        cluster.onRefresh(referers);
    }

    public Cluster<T> getCluster() {
        return cluster;
    }

    public URL getUrl() {
        return url;
    }

    private String getIdentities(List<URL> urls) {
        if (urls == null || urls.isEmpty()) {
            return "[]";
        }

        StringBuilder builder = new StringBuilder();
        builder.append("[");
        for (URL u : urls) {
            builder.append(u.getIdentity()).append(",");
        }
        builder.setLength(builder.length() - 1);
        builder.append("]");

        return builder.toString();
    }

    @SuppressWarnings("unchecked")
    private void prepareCluster() {
        String clusterName = url.getParameter(URLParam.CLUSTER.getName(), URLParam.CLUSTER.getValue());
        String loadbalanceName = url.getParameter(URLParam.LOADBALANCE.getName(), URLParam.LOADBALANCE.getValue());
        String haStrategyName = url.getParameter(URLParam.HA_STRATEGY.getName(), URLParam.HA_STRATEGY.getValue());

        cluster = ExtensionLoader.getExtensionLoader(Cluster.class).getExtension(clusterName);
        LoadBalance<T> loadBalance = ExtensionLoader.getExtensionLoader(LoadBalance.class).getExtension(loadbalanceName);
        HaStrategy<T> ha = ExtensionLoader.getExtensionLoader(HaStrategy.class).getExtension(haStrategyName);
        cluster.setLoadBalance(loadBalance);
        cluster.setHaStrategy(ha);
        cluster.setUrl(url);
    }

    private List<URL> parseDirectUrls(String directUrlStr) {
        String[] durlArr = CommonConstant.COMMA_SPLIT_PATTERN.split(directUrlStr);
        List<URL> directUrls = new ArrayList<URL>();
        for (String dus : durlArr) {
            URL du = URL.valueOf(StringTools.urlDecode(dus));
            if (du != null) {
                directUrls.add(du);
            }
        }
        return directUrls;
    }

	
}
