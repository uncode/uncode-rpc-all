package cn.uncode.rpc.config;

/**
 * 
 * 扩展配置的扩展点，需要在refererConfig和serviceConfig中增加更多配置的应用，通过此类扩展。
 *
 */

public class ExtConfig extends AbstractConfig {

	private static final long serialVersionUID = 1L;

}
