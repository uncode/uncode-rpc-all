package cn.uncode.rpc.transport.support;

import java.net.InetSocketAddress;

import cn.uncode.rpc.common.ChannelState;
import cn.uncode.rpc.core.URL;
import cn.uncode.rpc.core.URLParam;
import cn.uncode.rpc.spi.ExtensionLoader;
import cn.uncode.rpc.transport.Codec;
import cn.uncode.rpc.transport.Server;



/**
 * @author maijunsheng
 * @version 创建时间：2013-5-21
 * 
 */
public abstract class AbstractServer implements Server {
	
    protected InetSocketAddress localAddress;
    protected InetSocketAddress remoteAddress;

    protected URL url;
    protected Codec codec;

    protected volatile ChannelState state = ChannelState.UNINIT;


    public AbstractServer() {}

    public AbstractServer(URL url) {
        this.url = url;
		this.codec = ExtensionLoader.getExtensionLoader(Codec.class)
				.getExtension(url.getParameter(URLParam.CODEC.getName(), URLParam.CODEC.getValue()));
    }

    public InetSocketAddress getLocalAddress() {
        return localAddress;
    }

    public InetSocketAddress getRemoteAddress() {
        return remoteAddress;
    }

    public void setLocalAddress(InetSocketAddress localAddress) {
        this.localAddress = localAddress;
    }

    public void setRemoteAddress(InetSocketAddress remoteAddress) {
        this.remoteAddress = remoteAddress;
    }

    public void setUrl(URL url) {
        this.url = url;
    }

    public void setCodec(Codec codec) {
        this.codec = codec;
    }

}
