package cn.uncode.rpc.util;

import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import cn.uncode.rpc.common.log.Logger;
import cn.uncode.rpc.common.log.LoggerFactory;
import cn.uncode.rpc.core.Invoker;


public class InvokerUtil {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(InvokerUtil.class);

    private static ScheduledExecutorService scheduledExecutor = Executors.newScheduledThreadPool(10);

    // 正常情况下请求超过1s已经是能够忍耐的极限值了，delay 1s进行destroy
    private static final int DELAY_TIME = 1000;

    public static <T> void delayDestroy(final List<Invoker<T>> referers) {
        if (referers == null || referers.isEmpty()) {
            return;
        }

        scheduledExecutor.schedule(new Runnable() {
            @Override
            public void run() {

                for (Invoker<?> referer : referers) {
                    try {
                        referer.destroy();
                    } catch (Exception e) {
                    	LOGGER.error("RefererSupports delayDestroy Error: url=" + referer.getUrl().getUri(), e);
                    }
                }
            }
        }, DELAY_TIME, TimeUnit.MILLISECONDS);

        LOGGER.info("RefererSupports delayDestroy Success: size={} service={} urls={}", referers.size(), referers.get(0).getUrl()
                .getIdentity(), getServerPorts(referers));
    }

    private static <T> String getServerPorts(List<Invoker<T>> referers) {
        if (referers == null || referers.isEmpty()) {
            return "[]";
        }

        StringBuilder builder = new StringBuilder();
        builder.append("[");
        for (Invoker<T> referer : referers) {
            builder.append(referer.getUrl().getServerPortStr()).append(",");
        }
        builder.setLength(builder.length() - 1);
        builder.append("]");

        return builder.toString();
    }
}
