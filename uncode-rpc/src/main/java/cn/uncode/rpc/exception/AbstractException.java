package cn.uncode.rpc.exception;

import cn.uncode.rpc.common.message.Message;

public abstract class AbstractException extends RuntimeException {

	private static final long serialVersionUID = -5256995698362798973L;
	
	protected Message msg = null;

    public AbstractException() {
        super();
    }
    
    public AbstractException(String msg) {
        super(msg);
    }

    public AbstractException(Message msg) {
        super(msg.message);
        this.msg = msg;
    }
    
    public AbstractException(Throwable cause) {
        super(cause);
    }
    
    public AbstractException(Message msg, Throwable cause) {
        super(msg.message, cause);
        this.msg = msg;
    }
    
    public AbstractException(String msg, Throwable cause) {
        super(msg, cause);
    }


    @Override
    public String getMessage() {
        if (msg == null) {
            return super.getMessage();
        }
        return msg.message;
    }

    public Message getErrorMsg() {
        return msg;
    }
}
