/*
 *  Copyright 2009-2016 Weibo, Inc.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package cn.uncode.rpc.transport.netty;

import java.net.InetSocketAddress;
import java.net.SocketAddress;

import cn.uncode.rpc.common.CommonConstant;
import cn.uncode.rpc.common.log.Logger;
import cn.uncode.rpc.common.log.LoggerFactory;
import cn.uncode.rpc.core.DefaultResponse;
import cn.uncode.rpc.core.Response;
import cn.uncode.rpc.transport.Codec;
import io.netty.buffer.ByteBuf;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.LengthFieldBasedFrameDecoder;



/**
 * netty client decode
 */
public class NettyDecoder extends LengthFieldBasedFrameDecoder {
	
	
	private static final Logger LOGGER = LoggerFactory.getLogger(NettyDecoder.class);

	private Codec codec;
	private cn.uncode.rpc.transport.Channel client;
	private int maxContentLength = 0;

	public NettyDecoder(Codec codec, int maxContentLength) {
		super(Integer.MAX_VALUE, 0, maxContentLength);
		this.codec = codec;
		this.maxContentLength = maxContentLength;
	}

	protected Object decode(ChannelHandlerContext ctx, ByteBuf buffer) throws Exception {
		if (buffer.readableBytes() <= CommonConstant.NETTY_HEADER) {
			return null;
		}

		buffer.markReaderIndex();

		short type = buffer.readShort();
		
		if (type != CommonConstant.NETTY_MAGIC_TYPE) {
			buffer.resetReaderIndex();
			throw new NettyException("NettyDecoder transport header not support, type: " + type);
		}

		byte messageType = (byte) buffer.readShort();
		long requestId = buffer.readLong();

		int dataLength = buffer.readInt();

		// FIXME 如果dataLength过大，可能导致问题
		if (buffer.readableBytes() < dataLength) {
			buffer.resetReaderIndex();
			return null;
		}
		

		if (maxContentLength > 0 && dataLength > maxContentLength) {
			LOGGER.warn(
					"NettyDecoder transport data content length over of limit, size: {}  > {}. remote={} local={}",
					dataLength, maxContentLength, ctx.channel().remoteAddress(), ctx.channel().localAddress());
			Exception e = new NettyException("NettyDecoder transport data content length over of limit, size: "
					+ dataLength + " > " + maxContentLength);
			
			if (messageType == CommonConstant.FLAG_REQUEST) {
				return  buildExceptionResponse(requestId, e);
			} else {
				throw e;
			}
		}

		byte[] data = new byte[dataLength];

		buffer.readBytes(data);

		try {
		    String remoteIp = getRemoteIp(ctx.channel());
			return codec.decode(client, remoteIp, data);
		} catch (Exception e) {
			if (messageType == CommonConstant.FLAG_REQUEST) {
				return  buildExceptionResponse(requestId, e);
			} else {
				throw e;
			}
		}
	}

	private Response buildExceptionResponse(long requestId, Exception e) {
		DefaultResponse response = new DefaultResponse();
		response.setRequestId(requestId);
		response.setException(e);
		return response;
	}
	
	
    private String getRemoteIp(Channel channel) {
        String ip = "";
        SocketAddress remote = channel.remoteAddress();
        if (remote != null) {
            try {
                ip = ((InetSocketAddress) remote).getAddress().getHostAddress();
            } catch (Exception e) {
                LOGGER.warn("get remoteIp error!dedault will use. msg:" + e.getMessage() + ", remote:" + remote.toString());
            }
        }
        return ip;

    }
}
