package cn.uncode.rpc.exception;

import cn.uncode.rpc.common.message.Message;

/**
 * @author : juny.ye
 */
public class ClusterException extends AbstractException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public ClusterException() {
        super();
    }

    public ClusterException(Message msg) {
        super(msg);
    }
    
    public ClusterException(Message msg, Throwable cause) {
        super(msg, cause);
    }

    public ClusterException(Throwable cause) {
        super(cause);
    }
    
    public ClusterException(String msg) {
        super(msg);
    }
    
    public ClusterException(String msg, Throwable cause) {
        super(msg, cause);
    }

}
