/**
 * Copyright (C) 2016 Newland Group Holding Limited
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.uncode.rpc.transport.netty.client;

import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import cn.uncode.rpc.common.CommonConstant;
import cn.uncode.rpc.core.URL;
import cn.uncode.rpc.spi.ExtensionLoader;
import cn.uncode.rpc.transport.NettyChannelBuilder;

public class NettyClientChannelInitializer extends ChannelInitializer<SocketChannel> {

    private URL url;

    public NettyClientChannelInitializer(URL url) {
        this.url = url;
    }

    protected void initChannel(SocketChannel socketChannel) throws Exception {
        ChannelPipeline pipeline = socketChannel.pipeline();
        NettyChannelBuilder nettyChannelBuilder = ExtensionLoader.getExtensionLoader(NettyChannelBuilder.class).getExtension(CommonConstant.DEFAULT_VALUE);
        nettyChannelBuilder.buildClient(null, pipeline);
    }
}
