package cn.uncode.rpc.core;

public abstract class AbstractExporter<T> extends AbstractNode implements Exporter<T> {
	
	protected Invoker<T> invoker;

	public AbstractExporter(Invoker<T> invoker, URL url) {
		super(url);
		if (invoker == null)
            throw new IllegalStateException("service invoker == null");
        if (invoker.getInterface() == null)
            throw new IllegalStateException("service type == null");
        if (invoker.getUrl() == null)
            throw new IllegalStateException("service url == null");
		this.invoker = invoker;
	}
	
	
	public Invoker<T> getInvoker(){
		return invoker;
	}
	
	
	
	
	
	
	
	

}
