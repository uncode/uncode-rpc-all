package cn.uncode.rpc.exception;

import cn.uncode.rpc.common.message.Message;

/**
 * @author : juny.ye
 */
public class SpringSupportException extends AbstractException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public SpringSupportException() {
        super();
    }

    public SpringSupportException(Message msg) {
        super(msg);
    }
    
    public SpringSupportException(Message msg, Throwable cause) {
        super(msg, cause);
    }

    public SpringSupportException(Throwable cause) {
        super(cause);
    }
    
    public SpringSupportException(String msg) {
        super(msg);
    }
    
    public SpringSupportException(String msg, Throwable cause) {
        super(msg, cause);
    }

}
