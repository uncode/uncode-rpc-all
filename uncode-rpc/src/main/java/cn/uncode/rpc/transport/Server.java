package cn.uncode.rpc.transport;

import cn.uncode.rpc.core.URL;

public interface Server {

    /**
     * open the channel
     */
    boolean start();

    /**
     * close the channel.
     */
    void stop();
    
    
    boolean isAvailable();
    
    boolean isStop();
    
    URL getUrl();
    
    
//    InetSocketAddress getLocalAddress();
//
//    InetSocketAddress getRemoteAddress(); 



}
