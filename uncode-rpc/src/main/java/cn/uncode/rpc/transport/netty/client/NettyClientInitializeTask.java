/**
 * Copyright (C) 2016 Newland Group Holding Limited
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.uncode.rpc.transport.netty.client;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.socket.nio.NioSocketChannel;

import java.net.InetSocketAddress;
import java.util.concurrent.Callable;

import cn.uncode.rpc.core.URL;
import cn.uncode.rpc.transport.RequestHandler;

public class NettyClientInitializeTask implements Callable<Boolean> {

    private EventLoopGroup eventLoopGroup = null;
    private URL url = null;
    private RequestHandler requestHandler = null;

    NettyClientInitializeTask(EventLoopGroup eventLoopGroup, URL url, RequestHandler requestHandler) {
        this.eventLoopGroup = eventLoopGroup;
        this.url = url;
        this.requestHandler = requestHandler;
    }

    public Boolean call() {
        Bootstrap b = new Bootstrap();
        b.group(eventLoopGroup)
                .channel(NioSocketChannel.class).option(ChannelOption.SO_KEEPALIVE, true);
        b.handler(new NettyClientChannelInitializer(url));

        ChannelFuture channelFuture = b.bind(url.getHost(), url.getPort());
        channelFuture.addListener(new ChannelFutureListener() {
            public void operationComplete(final ChannelFuture channelFuture) throws Exception {
                if (channelFuture.isSuccess()) {
                	NettyClientHandler handler = channelFuture.channel().pipeline().get(NettyClientHandler.class);
                    RpcServerLoader.getInstance().setMessageSendHandler(handler);
                }
            }
        });
        return Boolean.TRUE;
    }
}
