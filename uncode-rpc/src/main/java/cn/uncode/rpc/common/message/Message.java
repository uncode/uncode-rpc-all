package cn.uncode.rpc.common.message;

import java.io.Serializable;
import java.text.MessageFormat;

import org.apache.commons.lang3.StringUtils;

public class Message implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private static final String SEPARATOR = "-";
	
	public int code;
	public String message;
	
	public Message(int code, String message){
		this.code = code;
		this.message = message;
	}
	
	public static Message valueOf(String msg){
		int codeVal = 0;
		String messageVal = null;
		if(StringUtils.isNotBlank(msg)){
			int index = msg.indexOf(SEPARATOR);
			messageVal = msg.substring(index + 1);
			if(index > 0){
				codeVal = Integer.parseInt(msg.substring(0, index));
			}
		}
		return new Message(codeVal, messageVal);
	}
	
	
	public String toString(){
		return MessageFormat.format("code: {0}, message: {1}", new Object[] {code, message});
	}
	
	

}
