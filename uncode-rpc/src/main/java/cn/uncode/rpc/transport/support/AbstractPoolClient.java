/*
 *  Copyright 2009-2016 Weibo, Inc.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package cn.uncode.rpc.transport.support;


import org.apache.commons.pool.BasePoolableObjectFactory;
import org.apache.commons.pool.PoolableObjectFactory;
import org.apache.commons.pool.impl.GenericObjectPool;

import cn.uncode.rpc.common.log.Logger;
import cn.uncode.rpc.common.log.LoggerFactory;
import cn.uncode.rpc.core.URL;
import cn.uncode.rpc.core.URLParam;
import cn.uncode.rpc.exception.TransportException;
import cn.uncode.rpc.transport.Channel;

/**
 * @author maijunsheng
 * @version 创建时间：2013-6-14
 */
public abstract class AbstractPoolClient extends AbstractClient {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(AbstractPoolClient.class);
	
    protected static long defaultMinEvictableIdleTimeMillis = (long) 1000 * 60 * 60;//默认链接空闲时间
    protected static long defaultSoftMinEvictableIdleTimeMillis = (long) 1000 * 60 * 10;//
    protected static long defaultTimeBetweenEvictionRunsMillis = (long) 1000 * 60 * 10;//默认回收周期
    protected GenericObjectPool<Channel> pool;
    protected GenericObjectPool.Config poolConfig;
    protected PoolableObjectFactory factory;

    public AbstractPoolClient(URL url) {
        super(url);
    }

    protected void initPool() {
        poolConfig = new GenericObjectPool.Config();
        poolConfig.minIdle =
                url.getIntParameter(URLParam.MIN_CLIENT_CONNECTION.getName(), URLParam.MIN_CLIENT_CONNECTION.getIntValue());
        poolConfig.maxIdle =
                url.getIntParameter(URLParam.MAX_CLIENT_CONNECTION.getName(), URLParam.MAX_CLIENT_CONNECTION.getIntValue());
        poolConfig.maxActive = poolConfig.maxIdle;
        poolConfig.maxWait = url.getIntParameter(URLParam.REQUEST_TIMEOUT.getName(), URLParam.REQUEST_TIMEOUT.getIntValue());
        poolConfig.lifo = url.getBooleanParameter(URLParam.POOL_LIFO.getName(), URLParam.POOL_LIFO.getBooleanValue());
        poolConfig.minEvictableIdleTimeMillis = defaultMinEvictableIdleTimeMillis;
        poolConfig.softMinEvictableIdleTimeMillis = defaultSoftMinEvictableIdleTimeMillis;
        poolConfig.timeBetweenEvictionRunsMillis = defaultTimeBetweenEvictionRunsMillis;
        factory = createChannelFactory();

        pool = new GenericObjectPool<Channel>(factory, poolConfig);

        boolean lazyInit = url.getBooleanParameter(URLParam.LAZY_INIT.getName(), URLParam.LAZY_INIT.getBooleanValue());

        if (!lazyInit) {
            for (int i = 0; i < poolConfig.minIdle; i++) {
                try {
                    pool.addObject();
                } catch (Exception e) {
                	LOGGER.error("NettyClient init pool create connect Error: url=" + url.getUri(), e);
                }
            }
        }
    }

    protected abstract BasePoolableObjectFactory createChannelFactory();

    protected Channel borrowObject() throws Exception {
        Channel nettyChannel = pool.borrowObject();

        if (nettyChannel != null && nettyChannel.isAvailable()) {
            return nettyChannel;
        }

        invalidateObject(nettyChannel);

        String errorMsg = this.getClass().getSimpleName() + " borrowObject Error: url=" + url.getUri();
        LOGGER.error(errorMsg);
        throw new TransportException(errorMsg);
    }

    protected void invalidateObject(Channel nettyChannel) {
        if (nettyChannel == null) {
            return;
        }
        try {
            pool.invalidateObject(nettyChannel);
        } catch (Exception ie) {
        	LOGGER.error(this.getClass().getSimpleName() + " invalidate client Error: url=" + url.getUri(), ie);
        }
    }

    protected void returnObject(Channel channel) {
        if (channel == null) {
            return;
        }

        try {
            pool.returnObject(channel);
        } catch (Exception ie) {
        	LOGGER.error(this.getClass().getSimpleName() + " return client Error: url=" + url.getUri(), ie);
        }
    }

}
