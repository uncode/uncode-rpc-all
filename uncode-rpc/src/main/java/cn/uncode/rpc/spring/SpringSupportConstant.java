package cn.uncode.rpc.spring;

public interface SpringSupportConstant {
	
	
	public static final String BEAN_DEFINITION_PARSER_ID = "id";
	public static final String BEAN_DEFINITION_PARSER_NAME = "name";
	public static final String BEAN_DEFINITION_PARSER_CLASS = "class";
	public static final String BEAN_DEFINITION_PARSER_METHOD_SET = "set";
	public static final String BEAN_DEFINITION_PARSER_REF = "ref";
	public static final String BEAN_DEFINITION_PARSER_PROTOCOL = "protocol";
	public static final String BEAN_DEFINITION_PARSER_PROTOCOLS = "protocols";
	public static final String BEAN_DEFINITION_PARSER_REGISTRY = "registry";
	public static final String BEAN_DEFINITION_PARSER_METHODS = "methods";
	public static final String BEAN_DEFINITION_PARSER_BASE_PROVIDER = "basicProvider";
	public static final String BEAN_DEFINITION_PARSER_BASE_CALLER = "basicCaller";
	public static final String BEAN_DEFINITION_PARSER_EXT_CONFIG = "extConfig";
	
	
	
	public static final String BEAN_DEFINITION_PARSER_SLIPT = "\\s*[,]+\\s*";

}
