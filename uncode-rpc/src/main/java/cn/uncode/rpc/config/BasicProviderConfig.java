package cn.uncode.rpc.config;

public class BasicProviderConfig extends AbstractServiceConfig {

    private static final long serialVersionUID = -3342374271064293224L;
    
    
    /** 是否默认配置 */
    private Boolean isDefault;

    public void setDefault(boolean isDefault) {
        this.isDefault = isDefault;
    }

    public Boolean isDefault() {
        return isDefault;
    }

}
