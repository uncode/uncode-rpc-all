package cn.uncode.rpc.cluster;

import cn.uncode.rpc.core.Request;
import cn.uncode.rpc.core.Response;
import cn.uncode.rpc.core.URL;
import cn.uncode.rpc.spi.Scope;
import cn.uncode.rpc.spi.Spi;

/**
 * 
 * Ha strategy.
 *
 * @author fishermen
 * @version V1.0 created at: 2013-5-21
 */
@Spi(scope = Scope.PROTOTYPE)
public interface HaStrategy<T> {

    void setUrl(URL url);

    Response call(Request request, LoadBalance<T> loadBalance);

}
