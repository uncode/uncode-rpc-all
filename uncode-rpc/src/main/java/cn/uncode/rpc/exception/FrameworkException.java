package cn.uncode.rpc.exception;

import cn.uncode.rpc.common.message.Message;

/**
 * @author : juny.ye
 */
public class FrameworkException extends AbstractException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public FrameworkException() {
        super();
    }

    public FrameworkException(Message msg) {
        super(msg);
    }
    
    public FrameworkException(Message msg, Throwable cause) {
        super(msg, cause);
    }

    public FrameworkException(Throwable cause) {
        super(cause);
    }
    
    public FrameworkException(String msg) {
        super(msg);
    }
    
    public FrameworkException(String msg, Throwable cause) {
        super(msg, cause);
    }

}
