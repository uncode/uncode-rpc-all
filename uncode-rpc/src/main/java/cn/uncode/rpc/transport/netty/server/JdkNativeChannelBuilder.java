package cn.uncode.rpc.transport.netty.server;


import cn.uncode.rpc.common.CommonConstant;
import cn.uncode.rpc.core.URL;
import cn.uncode.rpc.spi.SpiMeta;
import cn.uncode.rpc.transport.NettyChannelBuilder;
import cn.uncode.rpc.transport.RequestHandler;
import cn.uncode.rpc.transport.netty.NettyServerHandler;
import cn.uncode.rpc.util.StandardThreadExecutor;
import io.netty.channel.ChannelPipeline;
import io.netty.handler.codec.LengthFieldBasedFrameDecoder;
import io.netty.handler.codec.LengthFieldPrepender;
import io.netty.handler.codec.serialization.ClassResolvers;
import io.netty.handler.codec.serialization.ObjectDecoder;
import io.netty.handler.codec.serialization.ObjectEncoder;




@SpiMeta(name = CommonConstant.DEFAULT_VALUE)
public class JdkNativeChannelBuilder implements NettyChannelBuilder {
    public void buildServer(StandardThreadExecutor standardThreadExecutor, RequestHandler requestHandler, ChannelPipeline pipeline) {
        pipeline.addLast(new LengthFieldBasedFrameDecoder(Integer.MAX_VALUE, 0, MESSAGE_LENGTH, 0, MESSAGE_LENGTH));
        pipeline.addLast(new LengthFieldPrepender(MESSAGE_LENGTH));
        pipeline.addLast(new ObjectEncoder());
        pipeline.addLast(new ObjectDecoder(Integer.MAX_VALUE, ClassResolvers.weakCachingConcurrentResolver(this.getClass().getClassLoader())));
        pipeline.addLast(new NettyServerHandler(standardThreadExecutor, requestHandler));
    }

	@Override
	public void buildClient(URL url, ChannelPipeline pipeline) {
		// TODO Auto-generated method stub
		
	}


}

