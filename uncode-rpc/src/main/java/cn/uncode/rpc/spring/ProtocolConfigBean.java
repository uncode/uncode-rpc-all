package cn.uncode.rpc.spring;

import org.springframework.beans.factory.BeanNameAware;

import cn.uncode.rpc.config.ProtocolConfig;




/**
 * @author fld
 *
 * Created by fld on 16/5/13.
 */
public class ProtocolConfigBean extends ProtocolConfig implements BeanNameAware {

    /**
	 * 
	 */
	private static final long serialVersionUID = -2204809736067852901L;

	@Override
    public void setBeanName(String name) {
        setId(name);
        UncodeNamespaceHandler.protocolDefineNames.add(name);
    }
}
