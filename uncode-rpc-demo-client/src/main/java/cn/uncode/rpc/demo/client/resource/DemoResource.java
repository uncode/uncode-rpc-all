package cn.uncode.rpc.demo.client.resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.ContextLoader;

import cn.uncode.rpc.demo.api.DemoService;
import cn.uncode.rpc.demo.client.service.Hello;
import cn.uncode.rpc.spring.annotation.Caller;


@Controller
@RequestMapping("/demo")
public class DemoResource {
	
	@Autowired
    private Hello hello;

	
	@RequestMapping(value = "/index")
    public ResponseEntity<Object> delete() throws Exception {
    	String result = hello.say();
        return new ResponseEntity<Object>(result, HttpStatus.OK);
    }

}
