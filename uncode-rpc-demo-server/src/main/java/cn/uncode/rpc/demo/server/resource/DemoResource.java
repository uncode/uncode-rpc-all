package cn.uncode.rpc.demo.server.resource;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;


@Controller
@RequestMapping("/demo")
public class DemoResource {
	

	
	@RequestMapping(value = "/index")
    public ResponseEntity<Object> delete() throws Exception {
        return new ResponseEntity<Object>("", HttpStatus.OK);
    }

}
