package cn.uncode.rpc.spring;

import org.springframework.beans.factory.BeanNameAware;

import cn.uncode.rpc.config.RegistryConfig;


/**
 * @author fld
 *
 * Created by fld on 16/5/13.
 */
public class RegistryConfigBean extends RegistryConfig implements BeanNameAware {

    /**
	 * 
	 */
	private static final long serialVersionUID = -5355675015016262853L;

	@Override
    public void setBeanName(String name) {
        setId(name);
        setName(name);
        UncodeNamespaceHandler.registryDefineNames.add(name);
    }

}
