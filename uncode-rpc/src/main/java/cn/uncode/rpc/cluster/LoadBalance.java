package cn.uncode.rpc.cluster;

import java.util.List;

import cn.uncode.rpc.core.Invoker;
import cn.uncode.rpc.core.Request;
import cn.uncode.rpc.spi.Scope;
import cn.uncode.rpc.spi.Spi;


/**
 * 
 * Loadbalance for select caller
 */
@Spi(scope = Scope.PROTOTYPE)
public interface LoadBalance<T> {

    void onRefresh(List<Invoker<T>> invokers);

    Invoker<T> select(Request request);

    void selectToHolder(Request request, List<Invoker<T>> invokersHolder);

    void setWeightString(String weightString);

}
