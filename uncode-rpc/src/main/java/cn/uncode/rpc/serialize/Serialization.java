package cn.uncode.rpc.serialize;

import java.io.IOException;

import cn.uncode.rpc.spi.Scope;
import cn.uncode.rpc.spi.Spi;

@Spi(scope=Scope.PROTOTYPE)
public interface Serialization {

	byte[] serialize(Object obj) throws IOException;

	<T> T deserialize(byte[] bytes, Class<T> clz) throws IOException;
}
