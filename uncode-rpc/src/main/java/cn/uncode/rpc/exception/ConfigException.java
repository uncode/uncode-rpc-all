package cn.uncode.rpc.exception;

import cn.uncode.rpc.common.message.Message;

/**
 * @author : juny.ye
 */
public class ConfigException extends AbstractException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public ConfigException() {
        super();
    }

    public ConfigException(Message msg) {
        super(msg);
    }
    
    public ConfigException(Message msg, Throwable cause) {
        super(msg, cause);
    }

    public ConfigException(Throwable cause) {
        super(cause);
    }
    
    public ConfigException(String msg) {
        super(msg);
    }
    
    public ConfigException(String msg, Throwable cause) {
        super(msg, cause);
    }

}
