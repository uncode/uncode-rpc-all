package cn.uncode.rpc.common.log.slf4j;

import java.io.File;

import cn.uncode.rpc.common.log.Level;
import cn.uncode.rpc.common.log.Logger;
import cn.uncode.rpc.common.log.LoggerAdapter;
import cn.uncode.rpc.spi.SpiMeta;


@SpiMeta(name="slf4j", sequence = 100)
public class Slf4jLoggerAdapter implements LoggerAdapter {
    
	public Logger getLogger(String key) {
		return new Slf4jLogger(org.slf4j.LoggerFactory.getLogger(key));
	}

    public Logger getLogger(Class<?> key) {
        return new Slf4jLogger(org.slf4j.LoggerFactory.getLogger(key));
    }

    private Level level;
    
    private File file;

    public void setLevel(Level level) {
        this.level = level;
    }

    public Level getLevel() {
        return level;
    }

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }

}
